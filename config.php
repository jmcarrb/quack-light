<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$config = parse_ini_file("config.ini", true); // load and parse configs
$db = new mysqli($config['database']['host'], $config['database']['username'],
        $config['database']['password'], $config['database']['database']);

if($db->connect_error){
    die($config['quack']['software_name'] . ' could not connect to the database');
}